import type { FC } from 'react'
import styled from 'styled-components'
import { Image as ImageC, Icon, IconEnumName } from '../../components'
import { useStateContext } from '../../context'
import { ActionType } from '../../context/reducer'

const Wrapper = styled.div`
	max-width: 1200px;
	width: 100%;
	margin: 0 auto;
	padding: 10px 25px;

	${({ theme }) => theme.breakpoints.mobile} {
		padding: 35px 80px 40px;
	}
`

const Title = styled.h2`
	position: relative;
	color: ${({ theme }) => theme.colors.oldSilver};
	font-size: ${({ theme }) => theme.fontSize.medium};
	display: flex;
	align-items: center;
`

const Line = styled.span`
	border-top: 1px solid ${({ theme }) => theme.colors.charlestonGreen};
	flex: 1;
	margin-left: 15px;
`

const List = styled.ul`
	display: grid;
	flex-wrap: wrap;
	gap: 50px;
	grid-template-columns: repeat(2, 1fr);
	padding-top: 26px;

	${({ theme }) => theme.breakpoints.mobile} {
		grid-template-columns: repeat(3, 1fr);
	}

	${({ theme }) => theme.breakpoints.tablet} {
		grid-template-columns: repeat(5, 1fr);
	}
	${({ theme }) => theme.breakpoints.desktop} {
		grid-template-columns: repeat(6, 1fr);
	}
`

const FavouriteItem = styled.div`
	display: none;
	position: absolute;
	top: 0;
	right: 0;
	width: 30px;
	height: 30px;
	background-color: ${({ theme }) => theme.colors.white};
	border-radius: 100%;
	z-index: 5;
	align-items: center;
	justify-content: center;
`

const Item = styled.li`
	position: relative;
	display: flex;
	position: relative;
	align-items: center;
	flex-direction: column;
	cursor: pointer;
	z-index: 4;

	&:hover {
		${FavouriteItem} {
			display: flex;
		}
	}
`

const WrapperImage = styled.div`
	width: 100%;
	position: relative;

	&:after {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		content: '';
		background-color: ${({ theme }) => theme.colors.white};
		opacity: 0.8;
		border-radius: 100%;
		z-index: 1;
	}
`

const Image = styled(ImageC)`
	position: relative;
	z-index: 2;
`

const Description = styled.p`
	color: ${({ theme }) => theme.colors.white};
	font-size: ${({ theme }) => theme.fontSize.xxsmall};
	margin-top: 15px;
`

const IconFavourite = styled(Icon)<{ active: boolean }>`
	background-color: ${({ active, theme }) => active && theme.colors.gradientCapri};
`

interface VoiceProps {
	id: string
	name: string
	icon: string
	tags: string[]
}

interface FavoriteActionProps {
	voice: VoiceProps
	isFavouriteList: boolean
}

const FavoriteAction: FC<FavoriteActionProps> = ({ voice, isFavouriteList }) => {
	const {
		state: { favoriteVoices },
		dispatch,
	} = useStateContext()

	const isActive = Boolean(favoriteVoices?.find(f => f.id === voice.id))

	return (
		<FavouriteItem
			onClick={() => {
				if (!isFavouriteList) {
					dispatch({
						action: isActive ? ActionType.REMOVE_FAVORITE_VOICE : ActionType.ADD_FAVORITE_VOICE,
						payload: voice,
					})
				} else {
					dispatch({ action: ActionType.REMOVE_FAVORITE_VOICE, payload: voice })
				}
			}}
		>
			<IconFavourite name={isActive ? IconEnumName.VOICE_FAVOURITE_OFF : IconEnumName.VOICE_FAVOURITE} active={isActive} />
		</FavouriteItem>
	)
}

interface ListVoicesProps {
	title?: string
	isFavouriteList?: boolean
	list?: VoiceProps[]
}

export const ListVoices: FC<ListVoicesProps> = ({ title, list, isFavouriteList = false }) => (
	<Wrapper>
		{title && (
			<Title>
				{title}
				<Line />
			</Title>
		)}
		<List>
			{list?.map(({ id, icon, name, tags }) => (
				<Item key={id}>
					<FavoriteAction voice={{ id, icon, name, tags }} isFavouriteList={isFavouriteList} />
					<WrapperImage>
						<Image src={`/images/${icon}`} width={110} height={110} alt={name} />
					</WrapperImage>
					<Description>{name}</Description>
				</Item>
			))}
		</List>
	</Wrapper>
)
