import { FC, useState } from 'react'
import styled from 'styled-components'
import { SearchBar as SearchBarC, DropdownFilter as DropdownFilterC, IconEnumName, Icon, Dropdown as DropdownC } from '../../components'
import { useStateContext } from '../../context'
import { ActionType } from '../../context/reducer'

const Wrapper = styled.header`
	padding: 13px 25px 15px;
	max-width: 1200px;
	width: 100%;
	margin: 0 auto;

	${({ theme }) => theme.breakpoints.tablet} {
		display: flex;
		align-items: center;
		flex-wrap: wrap;
	} ;
`

const SearchBar = styled(SearchBarC)`
	margin-bottom: 10px;

	${({ theme }) => theme.breakpoints.tablet} {
		max-width: 243px;
		margin-bottom: 0;
	}
`

const Spliter = styled.div`
	flex: 1;
`
const Dropdown = styled(DropdownC)``

const DropdownFilter = styled(DropdownFilterC)`
	margin-bottom: 10px;

	> div {
		width: 100%;
	}

	${({ theme }) => theme.breakpoints.tablet} {
		margin: 0 20px;

		> div {
			width: initial;
		}
	}
`

const WrapperPopular = styled.div`
	width: 100%;
	position: relative;
	padding: 0 45px 0 40px;

	${({ theme }) => theme.breakpoints.tablet} {
		width: initial;

		> div {
			width: 126px;
		}
	}
`

const IconRandom = styled(Icon)`
	cursor: pointer;
	position: absolute;
	right: 5px;
	top: 50%;
	transform: translateY(-50%);
`

const IconSort = styled(Icon)`
	cursor: pointer;
	position: absolute;
	left: 5px;
	top: 50%;
	transform: translateY(-50%);
}
`

export interface HeaderProps {
	className?: string
}

const mockOptionsPopular = ['Popular', 'less popular']

export const Header: FC<HeaderProps> = ({ className }) => {
	const {
		state: { allVoices },
		dispatch,
	} = useStateContext()
	const [sort, setSort] = useState(true)

	const handleSearch = (value: string) => {
		if (value.length === 0) {
			dispatch({ action: ActionType.ADD_ACTIVE_LIST, payload: [] })
			return
		}
		dispatch({ action: ActionType.ADD_ACTIVE_LIST, payload: allVoices.filter(v => v.name.includes(value)) })
	}

	const handleFilterByTag = (value: string) => {
		dispatch({ action: ActionType.ADD_ACTIVE_LIST, payload: allVoices.filter(v => v.tags.includes(value)) })
	}

	const handleFilterByPopular = (value: string) => {
		// TODO we don't have popular tag for every voice
		console.log(value)
	}

	const handleSort = () => {
		setSort(!sort)
		dispatch({
			action: ActionType.ADD_ACTIVE_LIST,
			payload: allVoices.sort((a, b) =>
				sort ? b.name.localeCompare(a.name, 'en', { sensitivity: 'base' }) : a.name.localeCompare(b.name, 'en', { sensitivity: 'base' }),
			),
		})
	}

	const handleRandom = () => {
		const getRandomNumber = () => Math.floor(Math.random() * allVoices.length)

		const randomVoices = []
		const randomNumbers: number[] = []

		for (let i = 0; i <= getRandomNumber(); i++) {
			const randomNumber = getRandomNumber()
			if (!randomNumbers?.includes(randomNumber)) {
				randomNumbers.push(randomNumber)
				randomVoices.push(allVoices[randomNumber])
			}
		}

		dispatch({
			action: ActionType.ADD_ACTIVE_LIST,
			payload: randomVoices,
		})
	}

	const createListTags = allVoices
		.map(v => v.tags)
		.flat(1)
		.reduce((total: string[], value: string) => (total.includes(value) ? total : [...total, value]), []) || ['']

	return (
		<Wrapper className={className}>
			<SearchBar handleSearch={handleSearch} />
			<Spliter />
			<DropdownFilter options={createListTags} icon={IconEnumName.EYE} handleDropdownActive={handleFilterByTag} />
			<WrapperPopular>
				<IconSort name={IconEnumName.SORT} onClick={handleSort} />
				<Dropdown options={mockOptionsPopular} handleDropdownActive={handleFilterByPopular} />
				<IconRandom name={IconEnumName.RANDOM} onClick={handleRandom} />
			</WrapperPopular>
		</Wrapper>
	)
}
