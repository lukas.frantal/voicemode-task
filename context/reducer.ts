import type { StateContext, VoiceProps } from '.'

export enum ActionType {
	ADD_FAVORITE_VOICE = 'Add favorite voice',
	REMOVE_FAVORITE_VOICE = 'Remove favorite voice',
	ADD_ACTIVE_LIST = 'Add active list',
}

export type DispatchProps =
	| { action: ActionType.ADD_FAVORITE_VOICE; payload: VoiceProps }
	| { action: ActionType.REMOVE_FAVORITE_VOICE; payload: VoiceProps }
	| { action: ActionType.ADD_ACTIVE_LIST; payload: VoiceProps[] }

export const reducer = (state: StateContext, dispatch: DispatchProps) => {
	switch (dispatch.action) {
		case ActionType.ADD_FAVORITE_VOICE:
			return {
				...state,
				favoriteVoices: [...state.favoriteVoices, { ...dispatch.payload }],
			}
		case ActionType.REMOVE_FAVORITE_VOICE:
			return {
				...state,
				favoriteVoices: state.favoriteVoices.filter(c => c.id !== dispatch.payload.id),
			}
		case ActionType.ADD_ACTIVE_LIST:
			return {
				...state,
				activeList: dispatch.payload,
			}
		default:
			throw new Error(`Not among actions ${JSON.stringify(dispatch)}`)
	}
}
