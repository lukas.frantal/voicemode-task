import React, { FC, useContext, useReducer, createContext, Dispatch } from 'react'
import { reducer, DispatchProps } from './reducer'

export interface VoiceProps {
	id: string
	name: string
	icon: string
	tags: string[]
}

export interface StateContext {
	allVoices: [] | VoiceProps[]
	favoriteVoices: [] | VoiceProps[]
	activeList: [] | VoiceProps[]
}

const defaultState: StateContext = {
	allVoices: [],
	favoriteVoices: [],
	activeList: [],
}

interface StoreProps {
	state: StateContext
	dispatch: Dispatch<DispatchProps>
}

const store = createContext<StoreProps>({ state: defaultState, dispatch: () => {} })
export const useStateContext = () => useContext(store)

interface StateProviderProps {
	allVoices: VoiceProps[]
}

export const StateProvider: FC<StateProviderProps> = ({ children, allVoices }) => {
	const [state, dispatch] = useReducer(reducer, { ...defaultState, allVoices })
	return <store.Provider value={{ state, dispatch }}>{children}</store.Provider>
}
