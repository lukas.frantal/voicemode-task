module.exports = {
	trailingComma: 'all',
	tabWidth: 2,
	singleQuote: true,
	arrowParens: 'avoid',
	semi: false,
	bracketSpacing: true,
	printWidth: 150,
	useTabs: true,
}
