import { FC, useRef, useState } from 'react'
import styled from 'styled-components'
import { Icon, IconEnumName, Input as InputC } from '..'

const Wrapper = styled.div`
	position: relative;
	width: 100%;
`

const Input = styled(InputC)`
	padding: 6px 35px 7px;
`

const IconSearch = styled(Icon)`
	position: absolute;
	cursor: pointer;
	top: 50%;
	transform: translateY(-50%);
	left: 5px;
	padding: 5px;
`

const IconCross = styled(Icon)`
	position: absolute;
	cursor: pointer;
	top: 50%;
	transform: translateY(-50%);
	right: 5px;
	padding: 5px;
`

interface SearchBarProps {
	handleSearch: (value: string) => void
	className?: string
}

export const SearchBar: FC<SearchBarProps> = ({ handleSearch, className }) => {
	const ref = useRef<HTMLInputElement>(null)
	const [value, setValue] = useState('')

	const handleOnChange = (e: { target: { value: any } }) => {
		setValue(e.target.value)
		handleSearch(e.target.value)
	}

	return (
		<Wrapper className={className}>
			<IconSearch name={IconEnumName.SEARCH} onClick={() => ref.current?.focus()} />
			<Input value={value} onChange={handleOnChange} reference={ref} />
			{value.length > 0 && (
				<IconCross
					name={IconEnumName.CROSS}
					onClick={() => {
						setValue('')
						handleSearch('')
					}}
				/>
			)}
		</Wrapper>
	)
}
