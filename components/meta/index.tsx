import type { FC } from 'react'
import Head from 'next/head'

export const Meta: FC = () => (
	<Head>
		<meta charSet="utf-8" />
		{/* <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png" /> */}
		{/* <link rel="manifest" href="/favicon/site.webmanifest" /> */}
		{/* <link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#000" /> */}
		{/* <link rel="shortcut icon" href="/favicon/favicon.ico" /> */}
		{/* <meta name="msapplication-TileColor" content="#000" /> */}
		{/* <meta name="msapplication-config" content="/favicon/browserconfig.xml" /> */}
		{/* <meta name="theme-color" content="#000" /> */}
		{/* <link rel="alternate" type="application/rss+xml" href="/feed.xml" /> */}
		{/* <meta property="og:image" content="/images/gohome.png" /> */}
		<meta name="viewport" content="initial-scale=1.0, width=device-width" />
		<title>voicmode</title>
	</Head>
)
