import type { FC } from 'react'
import { default as LinkNext } from 'next/link'
import styled from 'styled-components'

interface LinkProps {
	className?: string
	href: string
	locale?: string
	active?: boolean
	scroll?: boolean
	replaceHrefTo?: string
	target?: 'blank' | string
	onClick?: () => void
}

const Wrapper = styled.a<Pick<LinkProps, 'active'>>`
	display: inline-block;
	user-select: none;
	position: relative;
	font-weight: normal;
	font-size: inherit;
`

export const Link: FC<LinkProps> = ({ className, children, href, onClick, replaceHrefTo, scroll = true, active, locale, target }) => (
	<LinkNext href={href} as={replaceHrefTo} scroll={scroll} locale={locale} passHref>
		<Wrapper className={className} onClick={onClick} active={active} data-effect={children} target={target}>
			{children}
		</Wrapper>
	</LinkNext>
)
