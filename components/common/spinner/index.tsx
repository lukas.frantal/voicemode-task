import type { FC } from 'react'
import styled, { keyframes } from 'styled-components'

const spin = keyframes`
	0% {
    transform: rotate(0deg);
  }
	100% {
    transform: rotate(360deg);	
  }
`

const Wrapper = styled.span`
	display: inline-block;
	border-width: 16px;
	border-style: solid;
	border-color: ${({ theme }) => `${theme.colors.black} ${theme.colors.white} ${theme.colors.white}`};
	border-image: initial;
	border-radius: 50%;
	width: 120px;
	height: 120px;
	animation: 2s linear 0s infinite normal none running ${spin};

	${({ theme }) => theme.breakpoints.tv} {
		width: 10rem;
		height: 10rem;
		border-width: 1rem;
	}

	${({ theme }) => theme.breakpoints.tv5k} {
		border-width: 3rem;
		height: 30rem;
		width: 30rem;
	}
`

interface SpinnerProps {
	className?: string
}

export const Spinner: FC<SpinnerProps> = ({ className, children }) => {
	return <Wrapper className={className}>{children}</Wrapper>
}
