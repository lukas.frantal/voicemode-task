import type { FC } from 'react'
import styled from 'styled-components'
import Angle from '../../../public/images/icons/angle.svg'
import Eye from '../../../public/images/icons/eye.svg'
import Random from '../../../public/images/icons/random.svg'
import Search from '../../../public/images/icons/search.svg'
import Sort from '../../../public/images/icons/sort.svg'
import Cross from '../../../public/images/icons/cross.svg'
import VoiceFavouriteOff from '../../../public/images/icons/voice-favourite-off.svg'
import VoiceFavourite from '../../../public/images/icons/voice-favourite.svg'

const Wrapper = styled.i`
	display: inline-block;
`

export enum IconEnumName {
	ANGLE = 'angle',
	EYE = 'eye',
	RANDOM = 'random',
	SEARCH = 'search',
	SORT = 'sort',
	CROSS = 'cross',
	VOICE_FAVOURITE_OFF = 'voice-favourite-off',
	VOICE_FAVOURITE = 'voice-favourite',
}

interface IconProps {
	className?: string
	name: IconEnumName
	onClick?: () => void
}

const createIcon = (name: IconEnumName) => {
	switch (true) {
		case name === IconEnumName.ANGLE:
			return <Angle />
		case name === IconEnumName.EYE:
			return <Eye />
		case name === IconEnumName.RANDOM:
			return <Random />
		case name === IconEnumName.SEARCH:
			return <Search />
		case name === IconEnumName.SORT:
			return <Sort />
		case name === IconEnumName.CROSS:
			return <Cross />
		case name === IconEnumName.VOICE_FAVOURITE_OFF:
			return <VoiceFavouriteOff />
		case name === IconEnumName.VOICE_FAVOURITE:
			return <VoiceFavourite />
		default:
			throw new Error('You have to define correct IconEnumName')
	}
}

export const Icon: FC<IconProps> = ({ name, className = '', onClick }) => (
	<Wrapper onClick={onClick} className={className}>
		{createIcon(name)}
	</Wrapper>
)
