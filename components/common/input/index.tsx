import type { ChangeEvent, FC, RefObject } from 'react'
import styled from 'styled-components'

const Wrapper = styled.input`
	width: 100%;
	height: 32px;
	background-color: ${({ theme }) => theme.colors.black};
	color: ${({ theme }) => theme.colors.white};
	border: 1px solid ${({ theme }) => theme.colors.black};
	font-size: ${({ theme }) => theme.fontSize.xxsmall};
	border-radius: 18px;
	transition: all 0.2s;

	::placeholder {
		color: ${({ theme }) => theme.colors.white};
	}

	&:-webkit-autofill {
		-webkit-box-shadow: 0 0 0px 1000px black inset;
		background-color: ${({ theme }) => theme.colors.black} !important;
	}

	&:focus {
		outline: 0;
	}
`

export enum InputEnumType {
	TEXT = 'text',
	EMAIL = 'email',
	NUMBER = 'number',
	PASSWORD = 'password',
	TEL = 'tel',
	FILE = 'file',
}

export interface InputProps {
	type?: InputEnumType
	autocomplete?: string
	value?: string
	accept?: string
	multiple?: boolean
	disabled?: boolean
	id?: string
	placeholder?: string
	className?: string
	onChange?: (e: ChangeEvent<HTMLInputElement>) => void
	onFocus?: () => void
	onBlur?: () => void
	onClick?: () => void
	reference: RefObject<HTMLInputElement>
}

export const Input: FC<InputProps> = ({
	multiple,
	className,
	type = InputEnumType.TEXT,
	value,
	placeholder,
	onFocus,
	onBlur,
	onChange,
	onClick,
	disabled,
	reference,
}) => {
	return (
		<Wrapper
			ref={reference}
			multiple={multiple}
			disabled={disabled}
			className={className}
			onChange={onChange}
			type={type}
			value={value}
			placeholder={placeholder}
			onClick={onClick}
			onFocus={onFocus}
			onBlur={onBlur}
		/>
	)
}
