import { FC, useRef, useState } from 'react'
import styled from 'styled-components'
import { IconEnumName, Icon } from '..'
import { useClickOutside } from '../../../hooks'

const Wrapper = styled.div`
	position: relative;
	background-color: ${({ theme }) => theme.colors.black};
	color: ${({ theme }) => theme.colors.white};
	width: 100%;
	height: 32px;
	padding: 0 30px 0 20px;
	display: flex;
	align-items: center;
	cursor: pointer;
`

const Text = styled.span`
	opacity: 0.6;
	width: 100%;
	font-size: ${({ theme }) => theme.fontSize.xsmall};
	font-weight: ${({ theme }) => theme.fontWeight.normal};
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
`

const IconArrow = styled(Icon)<{ active: boolean }>`
	position: absolute;
	right: 8px;
	padding: 5px;
	top: 50%;
	transform: translateY(-50%) ${({ active }) => (active ? 'rotate(90deg)' : 'rotate(-90deg)')};
	transition: 0.2s all;
`

const Item = styled.li`
	opacity: 0.6;
	width: 100%;
	overflow: hidden;
	text-overflow: ellipsis;
	font-size: ${({ theme }) => theme.fontSize.xsmall};
	font-weight: ${({ theme }) => theme.fontWeight.normal};
	border: 1px solid ${({ theme }) => theme.colors.raisinBlack};
	padding: 0 20px;
	display: flex;
	align-items: center;
	height: 30px;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;

	&:hover {
		opacity: 1;
	}
`

const List = styled.ul`
	position: absolute;
	top: 100%;
	left: 0;
	width: 100%;
	background-color: ${({ theme }) => theme.colors.black};
	z-index: 100;
`

export interface DropdownProps {
	options: string[]
	className?: string
	handleDropdownActive: (value: string) => void
}

export const Dropdown: FC<DropdownProps> = ({ options, className, handleDropdownActive }) => {
	const [activeOption, setactiveOption] = useState(options[0])
	const [showOptions, setShowOptions] = useState(false)
	const ref = useRef<HTMLDivElement>(null)

	useClickOutside(ref, () => setShowOptions(false))

	return (
		<Wrapper ref={ref} className={className} onClick={() => setShowOptions(!showOptions)}>
			<Text>{activeOption}</Text>
			<IconArrow name={IconEnumName.ANGLE} active={showOptions} />
			{showOptions && (
				<List>
					{options.map(
						(o, index) =>
							o !== activeOption && (
								<Item
									key={index}
									onClick={() => {
										setactiveOption(o)
										handleDropdownActive(o)
									}}
								>
									{o}
								</Item>
							),
					)}
				</List>
			)}
		</Wrapper>
	)
}
