import type { FC } from 'react'
import styled from 'styled-components'
import Img from 'next/image'

const Wrapper = styled.div`
	position: relative;
	flex: 1;
	width: 100%;
	overflow: hidden;
`

interface ImageProps {
	className?: string
	src: string
	width: number
	height: number
	alt: string
}

const shimmer = (w: number, h: number) => `
<svg width="${w}" height="${h}" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
  <defs>
    <linearGradient id="g">
      <stop stop-color="#f1f1f1" offset="20%" />
      <stop stop-color="#f2f2f2" offset="50%" />
      <stop stop-color="#f3f3f3" offset="70%" />
    </linearGradient>
  </defs>
  <rect width="${w}" height="${h}" fill="#f3f3f3" />
  <rect id="r" width="${w}" height="${h}" fill="url(#g)" />
  <animate xlink:href="#r" attributeName="x" from="-${w}" to="${w}" dur="1s" repeatCount="indefinite"  />
</svg>`

const toBase64 = (str: string) => (typeof window === 'undefined' ? Buffer.from(str).toString('base64') : window.btoa(str))

export const Image: FC<ImageProps> = ({ src, className, width, height, alt }) => (
	<Wrapper className={className}>
		<Img
			src={src}
			layout="responsive"
			width={width}
			height={height}
			alt={alt}
			placeholder="blur"
			blurDataURL={`data:image/svg+xml;base64,${toBase64(shimmer(width, height))}`}
		/>
	</Wrapper>
)
