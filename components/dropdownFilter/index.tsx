import type { FC } from 'react'
import styled from 'styled-components'
import { IconEnumName, Icon as IconC, Dropdown, DropdownProps } from '..'

const Wrapper = styled.div`
	display: flex;
	position: relative;
	padding-left: 40px;
`

const Icon = styled(IconC)`
	position: absolute;
	left: 0;
	top: 50%;
	transform: translateY(-50%);
`

interface DropdownFilterProps extends DropdownProps {
	icon: IconEnumName
}

export const DropdownFilter: FC<DropdownFilterProps> = ({ options, icon, className, handleDropdownActive }) => {
	return (
		<Wrapper className={className}>
			<Icon name={icon} />
			<Dropdown options={options} handleDropdownActive={handleDropdownActive} />
		</Wrapper>
	)
}
