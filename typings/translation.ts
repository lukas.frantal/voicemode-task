export interface TranslationProps { 
  t: (value: string) => string 
}

export interface ILocalesProps {
	locale: string
}