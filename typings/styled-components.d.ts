import { theme } from '../styles/theme'

type CustomK1 = typeof theme.t1
type CustomK2 = typeof theme.t2

declare module 'styled-components' {
	export interface DefaultTheme extends CustomK2 extends CustomK1 {}
}
