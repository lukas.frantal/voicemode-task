import { fontFamily, colors, fontSize, breakpoints, fontWeight } from './base'

export const t2 = {
	fontFamily,
	colors,
	breakpoints,
	fontSize,
	fontWeight,
}
