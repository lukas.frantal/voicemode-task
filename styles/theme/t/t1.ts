import { fontFamily, colors, fontSize, breakpoints, fontWeight } from './base'

export const t1 = {
	fontFamily,
	colors,
	breakpoints,
	fontSize,
	fontWeight,
}
