export const breakpoints = {
	mobile: '@media screen and (min-width: 600px)',
	tablet: '@media screen and (min-width: 900px)',
	desktop: '@media screen and (min-width: 1024px)',
	tv: '@media screen and (min-width: 2000px)',
	tv5k: '@media screen and (min-width: 4000px)',
}

export const fontSize = {
	xxsmall: '14px',
	xsmall: '15px',
	small: '16px',
	medium: '18px',

	xmedium: '22px',
	large: '30px',
	xlarge: '36px',
	big: '40px',
	xbig: '4rem',
	huge: '7rem',
	xhuge: '9rem',
}

export const fontWeight = {
	bold: 700,
	normal: 500,
}

// color name by https://www.color-name.com/hex
export const colors = {
	white: '#ffffff',
	black: '#000000',
	eerieBlack: '#1B1B1B',
	oldSilver: '#858585',
	vividSkyBlue: '#00D9FF',
	raisinBlack: '#232323',
	charlestonGreen: '#2c2c2c',
	gradientCapri: 'linear-gradient(222.88deg, #00E3FF -5.16%, #00BBFF 91.4%);',
	gradientAqua: 'linear-gradient(225deg, #00EEFF 0%, #00BBFF 100%)',
}

export const fontFamily = 'Roboto, sans-serif'
