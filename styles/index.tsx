import type { FC } from 'react'
import { ThemeProvider, createGlobalStyle } from 'styled-components'
import { Reset } from 'styled-reset'
import { theme } from './theme'

export const GlobalStyle = createGlobalStyle` 
  html, body, body > div {
    font-family: ${({ theme }) => theme.fontFamily};
    height: 100vh;
    background-color: ${({ theme }) => theme.colors.eerieBlack};
    color: ${({ theme }) => theme.colors.white};
    font-weight: ${({ theme }) => theme.fontWeight.normal};
    overflow: hidden;
  }
	
	* {
		outline: 0;
		box-sizing: border-box;
	}

  a {
    color: inherit;
    display: inline-block;
    text-decoration: none;
  }

  a,
  label,
  button {
    cursor: pointer;
  }
`

export const Styler: FC = ({ children }) => (
	<ThemeProvider theme={theme.t1}>
		<Reset />
		<GlobalStyle />
		{children}
	</ThemeProvider>
)
