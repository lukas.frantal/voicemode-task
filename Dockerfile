FROM node:alpine

ENV PORT 3000

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Installing dependencies
COPY package.json .
RUN npm i
RUN npm i -g vercel

# Copying source files
COPY . .

# Building app
# RUN npm run build
EXPOSE 3000

RUN if [ "$ENV" = "LOCAL_DEV" ] ; then yarn dev ; fi
RUN if [ "$ENV" = "LOCAL_PROD" ] ; then yarn build && yarn start ; fi