import type { NextPage } from 'next'
import styled from 'styled-components'
import { Container as ContainerC, ContainerEnumDirection, ContainerEnumFlexPosition } from '../components'

const Wrapper = styled(ContainerC)`
	height: 100%;
	text-align: center;
`

const ErrorPage: NextPage = () => (
	<Wrapper direction={ContainerEnumDirection.COL} x={ContainerEnumFlexPosition.CENTER}>
		Error page
	</Wrapper>
)

export default ErrorPage
