// eslint-disable-next-line @next/next/no-document-import-in-page
import Document, { DocumentContext, DocumentInitialProps, Html } from 'next/document'
import React from 'react'
import { ServerStyleSheet } from 'styled-components'
import { Meta } from '../components'

class _document extends Document {
	static async getInitialProps(ctx: DocumentContext): Promise<DocumentInitialProps> {
		const sheet = new ServerStyleSheet()
		const originalRenderPage = ctx.renderPage

		try {
			ctx.renderPage = () =>
				originalRenderPage({
					enhanceApp: App => props => sheet.collectStyles(<App {...props} />),
					enhanceComponent: Component => Component,
				})

			const initialProps: DocumentInitialProps = await Document.getInitialProps(ctx)
			return {
				...initialProps,
				styles: (
					<Html>
						<Meta />
						{initialProps.styles}
						{sheet.getStyleElement()}
					</Html>
				),
			}
		} finally {
			sheet.seal()
		}
	}
}

export default _document
