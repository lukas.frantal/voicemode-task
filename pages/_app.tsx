import type { FC } from 'react'
import type { AppProps } from 'next/app'
import { appWithTranslation } from 'next-i18next'
import { Styler } from '../styles'
import nextI18NextConfig from '../next-i18next.config.js'
import { StateProvider } from '../context'
import { allVoices } from '../public/mock'

// STYLES FROM ICOMOON
import '../public/fonts/font.css'

const _app: FC<AppProps> = ({ Component, pageProps }) => (
	<Styler>
		<StateProvider allVoices={allVoices}>
			<Component {...pageProps} />
		</StateProvider>
	</Styler>
)

export default appWithTranslation(_app, nextI18NextConfig)
