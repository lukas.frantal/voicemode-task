import type { NextPage } from 'next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTranslation } from 'next-i18next'
import styled from 'styled-components'
import type { ILocalesProps } from '../typings/translation'
import { Header, ListVoices } from '../layout'
import { useStateContext } from '../context'

const Wrapper = styled.div`
	height: 100vh;
	overflow: hidden;
	display: flex;
	flex-direction: column;
`

const Section = styled.section`
	flex: 1;
	overflow-y: auto;
	overflow-x: hidden;
	-ms-overflow-style: none; /* IE and Edge */
	scrollbar-width: none; /* Firefox */

	/* Hide scrollbar for Chrome, Safari and Opera */
	&::-webkit-scrollbar {
		display: none;
	}
`

const locales = 'page'

const IndexPage: NextPage = () => {
	const { t } = useTranslation(locales)
	const {
		state: { favoriteVoices, allVoices, activeList },
	} = useStateContext()

	return (
		<Wrapper>
			<Header />
			<Section>
				{activeList.length > 0 ? (
					<ListVoices list={activeList} />
				) : (
					<>
						{favoriteVoices.length > 0 && <ListVoices title={t('section.titleFavorite')} list={favoriteVoices} isFavouriteList />}
						<ListVoices title={t('section.titleAll')} list={allVoices} />
					</>
				)}
			</Section>
		</Wrapper>
	)
}

export const getStaticProps = async ({ locale }: ILocalesProps): Promise<unknown> => ({
	props: {
		...(await serverSideTranslations(locale, [locales])),
	},
})

export default IndexPage
