# voicemode task

# [Demo](https://www.voicemode-task.lukasfrantal.com/)

## Prerequisites

you need install node, and yarn

- [node 16.x](https://nodejs.org/en/)
- [npm: 8.x](https://nodejs.org/en/)
- [yarn": 1.x](https://classic.yarnpkg.com/latest.msi)

- then install dependencies by command inside project path

```
yarn
```

## Commands

### Linter

if you want run linter you can use this command:

```
yarn lint
```

for auto fix for linter issues:

```
yarn lint:fix
```

### PROD mode

```
yarn build && yarn start
```

- run app with prod back-end mode

```
yarn build && yarn prod
```

### DEV mode

```
yarn dev
```

### Junit testing jest

```
yarn test
```
