import '@testing-library/jest-dom'

const defaultMatchMedia = {
	matches: false,
	addListener: jest.fn(),
	removeListener: jest.fn(),
	media: null,
	onchange: null,
	addEventListener: jest.fn(),
	removeEventListener: jest.fn(),
	dispatchEvent: jest.fn(),
}

Object.defineProperty(window, 'matchMedia', {
	writable: true,
	value: jest.fn().mockImplementation(() => defaultMatchMedia),
})

export const setMatchMediaProperties = (properties: Partial<MediaQueryList>): void => {
	global.matchMedia = () => Object.assign(defaultMatchMedia, properties)
}
